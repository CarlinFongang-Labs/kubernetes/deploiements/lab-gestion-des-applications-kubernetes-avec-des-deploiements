# 4. Lab : Mise à Jour et Évolutivité d'une Application Kubernetes
------------

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

# Objectifs

1. Mettre à jour l'application vers une nouvelle version du code


2. Adaptez l'application à un plus grand nombre de répliques

# Contexte

Vous travaillez pour une société appelée BeeHive, un service d'abonnement qui expédie des expéditions hebdomadaires d'abeilles aux clients. L'entreprise utilise Kubernetes pour exécuter son infrastructure d'applications conteneurisées.

L'une de ces applications est un simple serveur Web. Il est géré dans Kubernetes à l'aide d'un déploiement appelé beebox-web. Malheureusement, l'application rencontre quelques problèmes et ses performances sont médiocres sous une charge d'utilisateurs importante.

Deux étapes devront être suivies pour résoudre ce problème. Tout d’abord, vous devrez déployer une version plus récente de l’application ( 1.0.2) contenant certaines améliorations de performances apportées par les développeurs. Deuxièmement, vous devrez faire évoluer le déploiement de l'application, en augmentant le nombre de réplicas de 2à 5.

>![Alt text](img/image.png)


# Application


## Étape 1 : Connexion au Serveur du Nœud de Contrôle

Connectez-vous au serveur du nœud de contrôle à l'aide des informations d'identification fournies :

```sh
ssh -i id_rsa user@PUBLIC_IP_ADDRESS
```

En vérifiant les deployment et pods en cours, on a cette liste de départ

```bash
kubectl get deploy,po
```

>![Alt text](img/image-1.png)
*Deployment et Pods en cours*

## Étape 2 : Mise à Jour de l'Application vers une Nouvelle Version

1. Modifiez le déploiement `beebox-web` :

```sh
kubectl edit deployment beebox-web
```

2. Localisez la spécification du conteneur du Pod et remplacez la balise de version de l'image de `1.0.1` par `1.0.2` :

   ```yaml
   ...
   spec:
     containers:
     - image: acgorg/beebox-web:1.0.2
       imagePullPolicy: IfNotPresent
       name: web-server
   ...
   ```

3. Enregistrez et quittez le fichier en appuyant sur Échap suivi de `:wq`.

4. Vérifiez l'état de votre déploiement pour observer la mise à jour continue :

```sh
kubectl rollout status deployment.v1.apps/beebox-web
```

>![Alt text](img/image-2.png)
*Status du rolling update*

5. Description du deploiement

```sh
kubectl describe deploy beebox-web
```

>![Alt text](img/image-6.png)

## Étape 3 : Adapter l'application à un plus grand nombre de répliques

1. Adaptez le déploiement à 5 instances dupliquées :

```sh
kubectl scale deployment.v1.apps/beebox-web --replicas=5
```

>![Alt text](img/image-3.png)
*Scaling up réussi*

2. Visualisez le déploiement pour le voir évoluer :

```sh
kubectl get deployment beebox-web
```

>![Alt text](img/image-4.png)
*Mise à jour deployment*

3. Voir les pods :

```sh
kubectl get pods
```

>![Alt text](img/image-5.png)
*Mise à jour du nombre de pods*

Le nombre de pod à été mis à jour conformément à la mise à jour du deployement, notre scaling up est donc réussi